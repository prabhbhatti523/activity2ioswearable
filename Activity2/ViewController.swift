//
//  ViewController.swift
//  Activity2
//
//  Created by Prabhjinder Singh on 2019-10-29.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import UIKit
import Particle_SDK
import Speech


class ViewController: UIViewController, SFSpeechRecognizerDelegate {      
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    @IBOutlet weak var viewTextLabel: UILabel!
    
    
    @IBOutlet weak var startStopButton: UIButton!
    // MARK: User variables
    let USERNAME = "prabhjinderbhatti@gmail.com"
    let PASSWORD = "12345"
  
    let DEVICE_ID = "2e003e001047363333343437"
    var myPhoton : ParticleDevice?
    
    var flag = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        } // end login
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
               
            }
            
        } // end getDevice()
    }
    func recordAndRecognizeSpeech(){
        guard let node: AVAudioNode = audioEngine.inputNode else { return }
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat){
            buffer, _ in self.request.append(buffer)
        }
        audioEngine.prepare()
        do{
            try audioEngine.start()
        }catch{
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            return
        }
        if !myRecognizer.isAvailable{
            return
        }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                let bestString = result.bestTranscription.formattedString
                self.viewTextLabel.text = bestString
                print("okokokokok111112")
                if(bestString == "Yellow"){
                    print("okokokokok11111")
                    self.turnParticleYellow()
                }
                else if(bestString == "Green"){
                    self.turnParticleGreen()
                }
                
            }
            else if let error = error{
                print(error)
            }
        })
        
        
        
    }
    func turnParticleGreen() {
        let parameters = ["Green"]
        var task = myPhoton!.callFunction("changeTheColor", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
 }
    
    func turnParticleYellow() {
        let parameters = ["Yellow"]
        var task = myPhoton!.callFunction("changeTheColor", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to particle")
            }
            else {
                print("Error in yellow func")
            }
        }
   }

    @IBAction func speechToTextButton(_ sender: Any) {
        // recordAndRecognizeSpeech()
        if(flag == true){
             self.startStopButton.setTitle("STOP", for: .normal)
           recordAndRecognizeSpeech()
            self.flag = false
        print("started")

        }
     else if(flag == false){
            
            // stop recognition
            recognitionTask?.finish()
            recognitionTask = nil
            
            // stop audio
            request.endAudio()
            audioEngine.stop()
            audioEngine.inputNode.removeTap(onBus: 0)
            self.flag = true
            self.startStopButton.setTitle("START", for: .normal)
        print("stopped")
        }




    }
    
}

