// This #include statement was automatically added by the Particle IDE.
#include "lib1.h"

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();

void setup() {
  button.begin();

 
  Particle.function("changeTheColor", changeTheColor);

}
void loop() {

}
int changeTheColor(String cmd) {
  if (cmd == "Green") {
    button.allLedsOn(0,255,0);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "Yellow") {
    button.allLedsOn(255,255,0);
    delay(2000);
    button.allLedsOff();
  }
  else {
    // you received an invalid return -1
    return -1;
  }
  // function succesfully finished
  return 1;
}